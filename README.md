# IndexedDB CRUD



## Getting started

### what is IndexedDB ?
IndexedDB is a low-level API for client-side storage of significant amounts of structured data, including files/blobs. This API uses indexes to enable high-performance searches of this data. While Web Storage is useful for storing smaller amounts of data, it is less useful for storing larger amounts of structured data. IndexedDB provides a solution. This is the main landing page for MDN's IndexedDB coverage — here we provide links to the full API reference and usage guides, browser support details, and some explanation of key concepts.
# Init && CRUD Operations : 
## Init your DB and create a store (Collection/Table)
The following code creates a request for a database to be opened asynchronously, after which the database is opened when the request's onsuccess handler is fired:
 
 Once the DB is opened, we can create stores and define indexes.


```
const initDB = storeName => {
  return new Promise(resolve => {
    //Check if your browser support indexedDB
    if (!("indexedDB" in window)) {
      console.warn("This browser doesn't support IndexedDB.");
      resolve(false);
    }
    // open the connection
    request = indexedDB.open("myDB", version);
    request.onupgradeneeded = () => {
      db = request.result;
      // if the data object store doesn't exist, create it
      if (!db.objectStoreNames.contains(storeName)) {
        // in this case we used the id of items to insert as value of our keyPath
        const store = db.createObjectStore(storeName, { keyPath: "id" });
        // We create an index to make fetch data by key more fast
        store.createIndex("id", "id", { unique: true });
       
      }
      // no need to resolve here
    };
    //sucess store is created
    request.onsuccess = () => {
      db = request.result;
      version = db.version;
      resolve(true);
    };
    // fail to create a store 
    request.onerror = () => {
      resolve(false);
    };
  });
};
```

## Create : insert item to created store
The following code creates a request for a database to be opened asynchronously then inseret a item to the created store:
- if the request's onsuccess handler is fired : the item is inserted successfully .
- if the request's onerror handler is fired : fail to insert the item.
```
const addData = (storeName, data) => {
  return new Promise(resolve => {
    request = indexedDB.open("myDB", version);
    // Once the connection established we proceed to data insertion 
    request.onsuccess = () => {
      db = request.result;
      const tx = db.transaction(storeName, "readwrite");
      const store = tx.objectStore(storeName);
      const res = store.add(data);
      res.onsuccess = () => {
        resolve(res.result);
      };
    };
    // On Fail
    request.onerror = () => {
      const error = request.error?.message;
      if (error) {
        resolve(error);
      } else {
        resolve("Unknown error");
      }
    };
  });
};
```
## Read : read data from stores

### FindOne by keyPath:
The following code creates a request for a database to get a item based on his path key:

```
const findOne = (storeName, key) => {
  return new Promise(resolve => {
    request = indexedDB.open("myDB", version);

    request.onsuccess = () => {
      db = request.result;
      const tx = db.transaction(storeName, "readonly");
      const store = tx.objectStore(storeName);
      const res = store.get(key);
      res.onsuccess = () => {
        resolve(res.result);
      };
    };
  });
};

```
### Collect paginated data using cusrors :
The following code creates a request for a database to get paginated items according to sended params (limit,page) by using indexedDb cursor :

```
export const getStoreDataPaginated = async (
  storeName ="store",
  page,
  limit
) => {
  return new Promise(resolve => {
    request = indexedDB.open("myDB", version);
    request.onsuccess = () => {
      db = request.result;
      const tx = db.transaction(storeName, "readonly");
      const store = tx.objectStore(storeName);
      const data = [];
      const offset = parseInt((page - 1) * limit);
      const meta = {
        current_page: page,
        limit,
        offset
      };
      let hasSkipped = false;
      //Before all we count all items in our store to get more details about stored data
      store.count().onsuccess = res => {
        meta.total_items = res.target.result;
        meta.total_pages = Math.ceil(res.target.result / limit);
      };

      //Handle collect data by cusror according to limit and offset
      store.openCursor().onsuccess = function (e) {
        let cursor = e.target.result;
        if (cursor && !hasSkipped && offset > 0) {
          hasSkipped = true;
          cursor.advance(offset);
          return;
        }
        //Check if cursor is opned and there is more data to read 
        if (cursor) {
          //push results in array
          data.push(cursor.value);
          //if collected data is less than limit cursor should continue reading data
          if (data.length < limit) {
            cursor.continue();
          } else {
            // Once data is collected and there is no more data to read we return response
            resolve({
              success: true,
              data: {
                items: data,
                meta
              }
            });
          }
        } 
        //there is no more data according to params 
        else {
          resolve({
            success: true,
            data: {
              items: data,
              meta
            }
          });
        }
      };
    };
  });
};
```

## Update
### Find one and update
The following code creates a request for a database to update a item already inserted in the store :
- if the request's onsuccess handler is fired : the item is updated successfully .
- if the request's onerror handler is fired : fail to update item.
```
const findOneAndUpdate = (storeName, keyPath, changes) => {
  return new Promise(resolve => {
    request = indexedDB.open("myDB", version);

    request.onsuccess = () => {
      db = request.result;
      const tx = db.transaction(storeName, "readwrite");
      const store = tx.objectStore(storeName);
      const res = store.get(keyPath);
      res.onsuccess = () => {
        const item = res.result;
        const updateRequest = store.put({ ...item, ...changes });
        updateRequest.onsuccess = () => {
          resolve(updateRequest.result);
        };
      };
    };
  });
};
```
## Delete :
### Delete item based on keyPath
The following code creates a request for a database to delete item:
- if the request's onsuccess handler is fired : the item is deleted successfully .
- if the request's onerror handler is fired : fail to delete item.
```
const deleteData = (storeName, key) => {
  return new Promise(resolve => {
    // again open the connection
    request = indexedDB.open("myDB", version);
    request.onsuccess = () => {
      db = request.result;
      const tx = db.transaction(storeName, "readwrite");
      const store = tx.objectStore(storeName);
      const res = store.delete(key);
      // add listeners that will resolve the Promise
      res.onsuccess = () => {
        resolve(true);
      };
      res.onerror = () => {
        resolve(false);
      };
    };
  });
```
# Close Connection :
It's critical to appropriately manage the IndexedDB connection life cycle. To prevent resource leaks and enhance performance, open connections when needed and close them when not in use. 
```
// now let's close the database again!
  db.close();
```
# Conclusion
By exploring these resources, you’ll gain a solid understanding of IndexedDB and be well-equipped to utilize its features effectively in your web applications.
## Author
Mhenni Med Amine 


