let request;
let db;
let version = 1; //optional 
//This function is used to init DB and create à store (Collection/Table)
export const initDB = storeName => {
  return new Promise(resolve => {
    //Check if your browser support indexedDB
    if (!("indexedDB" in window)) {
      console.warn("This browser doesn't support IndexedDB.");
      resolve(false);
    }
    // open the connection
    request = indexedDB.open("myDB", version);
    request.onupgradeneeded = () => {
      db = request.result;
      // if the data object store doesn't exist, create it
      if (!db.objectStoreNames.contains(storeName)) {
        // in this case we used the id of items to insert as value of our keyPath
        const store = db.createObjectStore(storeName, { keyPath: "id" });
        // We create an index to make fetch data by key more fast
        store.createIndex("id", "id", { unique: true });
       
      }
      // no need to resolve here
    };
    //sucess store is created
    request.onsuccess = () => {
      db = request.result;
      version = db.version;
      resolve(true);
    };
    // fail to create a store 
    request.onerror = () => {
      resolve(false);
    };
  });
};
// This function is used to insert element to specific store
export const addData = (storeName, data) => {
  return new Promise(resolve => {
    request = indexedDB.open("myDB", version);
    // Once the connection established we proceed to data insertion 
    request.onsuccess = () => {
      db = request.result;
      const tx = db.transaction(storeName, "readwrite");
      const store = tx.objectStore(storeName);
      const res = store.add(data);
      res.onsuccess = () => {
        resolve(res.result);
      };
    };
    // On Fail
    request.onerror = () => {
      const error = request.error?.message;
      if (error) {
        resolve(error);
      } else {
        resolve("Unknown error");
      }
    };
  });
};
// This function is used to get data paginated (in this case we collect all Data then we handle pagination logic manually)
export const getStoreData = (
  storeName = "store",
  page ,
  limit,
) => {
  return new Promise(resolve => {
    request = indexedDB.open("myDB", version);

    request.onsuccess = () => {
      db = request.result;
      const tx = db.transaction(storeName, "readonly");
      const store = tx.objectStore(storeName);

      let data = [];
      const offset = parseInt((page - 1) * limit);
      const meta = {
        current_page: page,
        limit,
        offset
      };
      store.count().onsuccess = res => {
        meta.total_items = res.target.result;
        meta.total_pages = Math.ceil(res.target.result / limit);
      };
      const res = store.getAll();
      res.onsuccess = () => {
        if (res?.result?.length > 0) {
          data = res?.result;
        

          if (page && limit) {
            data = data
              .sort(
                (itemA, itemB) =>
                  new Date(itemB.created_at) -
                  new Date(itemA.created_at)
              )
              .slice(offset, offset + limit);
          } else data = res.result;
        }
        resolve({
          success: true,
          data: {
            items: data,
            meta
          }
        });
      };
    };
  });
};
// This function is used to get data paginated based on IndexedDb Cursors
export const getStoreDataPaginated = async (
  storeName ="store",
  page,
  limit
) => {
  return new Promise(resolve => {
    request = indexedDB.open("myDB", version);
    request.onsuccess = () => {
      db = request.result;
      const tx = db.transaction(storeName, "readonly");
      const store = tx.objectStore(storeName);
      const data = [];
      const offset = parseInt((page - 1) * limit);
      const meta = {
        current_page: page,
        limit,
        offset
      };
      let hasSkipped = false;
      //Before all we count all items in our store to get more details about stored data
      store.count().onsuccess = res => {
        meta.total_items = res.target.result;
        meta.total_pages = Math.ceil(res.target.result / limit);
      };

      //Handle collect data by cusror according to limit and offset
      store.openCursor().onsuccess = function (e) {
        let cursor = e.target.result;
        if (cursor && !hasSkipped && offset > 0) {
          hasSkipped = true;
          cursor.advance(offset);
          return;
        }
        //Check if cursor is opned and there is more data to read 
        if (cursor) {
          //push results in array
          data.push(cursor.value);
          //if collected data is less than limit cursor should continue reading data
          if (data.length < limit) {
            cursor.continue();
          } else {
            // Once data is collected and there is no more data to read we return response
            resolve({
              success: true,
              data: {
                items: data,
                meta
              }
            });
          }
        } 
        //there is no more data according to params 
        else {
          resolve({
            success: true,
            data: {
              items: data,
              meta
            }
          });
        }
      };
    };
  });
};
// Find one by defined key path in store
export const findOne = (storeName, value) => {
  return new Promise(resolve => {
    request = indexedDB.open("myDB", version);

    request.onsuccess = () => {
      db = request.result;
      const tx = db.transaction(storeName, "readonly");
      const store = tx.objectStore(storeName);
      const res = store.get(value);
      res.onsuccess = () => {
        resolve(res.result);
      };
    };
  });
};
// Find one and update
export const findOneAndUpdate = (storeName, keyPath, changes) => {
  return new Promise(resolve => {
    request = indexedDB.open("myDB", version);

    request.onsuccess = () => {
      db = request.result;
      const tx = db.transaction(storeName, "readwrite");
      const store = tx.objectStore(storeName);
      const res = store.get(keyPath);
      res.onsuccess = () => {
        const item = res.result;
        const updateRequest = store.put({ ...item, ...changes });
        updateRequest.onsuccess = () => {
          resolve(updateRequest.result);
        };
      };
    };
  });
};
//Delete Item by pathKey:
export const deleteData = (storeName, key) => {
  return new Promise(resolve => {
    // again open the connection
    request = indexedDB.open("myDB", version);
    request.onsuccess = () => {
      db = request.result;
      const tx = db.transaction(storeName, "readwrite");
      const store = tx.objectStore(storeName);
      const res = store.delete(key);
      // add listeners that will resolve the Promise
      res.onsuccess = () => {
        resolve(true);
      };
      res.onerror = () => {
        resolve(false);
      };
    };
  });
};
